# ⚠ RP Keyboard has moved to [GitHub](https://github.com/Arxareon/RPKeyboard). Look for new updates there!
If you wish to support the future development of RP Keyboard, you can now [Sponsor](https://github.com/sponsors/Arxareon) my work on GitHub as an option.

***Thank you for understanding!***

- - -
***A simple solution to a simple problem:*** Talk to other RP Keyboard users via the alphabet , runes and markings of several races of Warcraft.

## Features
* Talk to other RP Keyboard users via the alphabet , runes and markings of several races of Warcraft.

## Other Addons
*If you like minimalistic solutions, you might want to check out there Addons as well:*

[**Movement Speed**](https://bitbucket.org/Arxareon/movement-speed) • View the movement speed of anyone in customizable displays and tooltips with fast updates.

[**Remaining XP**](https://bitbucket.org/Arxareon/remaining-xp) • Shows the amount of XP needed to reach the next level & much more.

*Coming soon™:*
[**Party Targets**](https://bitbucket.org/Arxareon/party-targets) • See who your party and raid members are targeting.

## Support
*Possibilities to directly support the development of Party Targets and other addons:*

[![Donate on PayPal](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/PayPal.svg/124px-PayPal.svg.png)](https://www.paypal.com/donate/?hosted_button_id=Z4FSAFKA5LX98)

## Links
[**CurseForge**](https://www.curseforge.com/wow/addons/rp-keyboard) • Should you prefer, support development by watching ads on CurseForge, and keep up to date via automatic updates via the CurseForge App.

[**Wago**](https://addons.wago.io/addons/rp-keyboard) • You can also use Wago to get updates and support development by watching ads, and provide much appreciated continuous support via a Subscription.

*You can also get updates via **WoWUP** if you have Wago.io enabled as an addon provider!*

## License
All Rights Reserved unless otherwise explicitly stated.

- - -
Thank you for checking out Party Targets!
If you have any comments, wishes or problems, please, don't be afraid to share them. GLHF!

*Arxareon*